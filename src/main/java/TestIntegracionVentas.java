import dto.*;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import serializer.GsonSerializer;

import java.util.Properties;

public class TestIntegracionVentas {


    public static void main(String[] args) {
        Properties properties = new Properties();
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, GsonSerializer.class.getName());

        String topicTransaccion = "dev-streaming.cdp.dbo.Transacciones";
        String topicTransaccionVenta = "dev-streaming.cdp.dbo.TransaccionesVenta";
        String topicPago = "dev-streaming.cdp.dbo.Pagos";
        String topicItem = "streaming.cdp.dbo.Items";
        String topicMedioPago = "streaming.publish.mediospago-dev";
        String topicDependence = "streaming.maestradependencias-dev";

        int messagesNumber = 20;

       /* Producer<String, TransaccionDTO> producerTransaccion = new KafkaProducer<>(properties);
        Producer<String, TransaccionVentaDTO> producerTransaccionVenta = new KafkaProducer<>(properties);
        Producer<String, PagoDTO> producerPagos = new KafkaProducer<>(properties);*/
        Producer<String, ItemDTO> producerItems = new KafkaProducer<>(properties);
       /* Producer<String, DependeceDTO> producerMaestroDependencias = new KafkaProducer<>(properties);
        Producer<String, MedioPagoDTO> mediosPago = new KafkaProducer<>(properties);

        //Dependence
        DependeceDTO dependeceDTO=new DependeceDTO("0028","Exito San Nicolas","E");
        ProducerRecord<String,DependeceDTO> dependeceDTOProducerRecord=new ProducerRecord<>(topicDependence,dependeceDTO.getCodigoDepend(),dependeceDTO);
        producerMaestroDependencias.send(dependeceDTOProducerRecord);
        producerMaestroDependencias.close();
        //Medios de pago
        String[] pagos={"efectivo","Puntos","Tarjeta Exito"};
        int count=1;
        for (String pago:
             pagos) {
            MedioPagoDTO medioPagoDTO=createMedioPago(String.valueOf(count),pago);
            ProducerRecord<String, MedioPagoDTO> messageMedioPago = new ProducerRecord<>(topicMedioPago, medioPagoDTO.getIdentificadorMedioPago(), medioPagoDTO);
            mediosPago.send(messageMedioPago);
            count++;
        }
        mediosPago.close();*/
        //Transacciones
        for (int i = 0; i < messagesNumber; i++) {
            /*System.out.println(i);
            TransaccionDTO transaccionDTO = createTransaccion(String.valueOf(i));
            TransaccionVentaDTO transaccionVentaDTO = createTransaccionVenta(String.valueOf(i));
            //Pagos
            int numPagos = ((int) (Math.random() * 3)) + 1;
            for (int j = 1; j <= numPagos; j++) {
                PagoDTO pagoDTO = createPago(String.valueOf(i), String.valueOf(j));
                ProducerRecord<String, PagoDTO> messagePago = new ProducerRecord<>(topicPago, pagoDTO.getTransaccion().concat("-pagos"), pagoDTO);
                producerPagos.send(messagePago);

            }
*/
            //Items
            int numItems = ((int) (Math.random() * 30)) + 1;
            for (int j = 1; j <= 1; j++) {
                ItemDTO itemDTO=createItem(String.valueOf(i),String.valueOf(j),String.valueOf(j+123));
                ProducerRecord<String, ItemDTO> messageItems = new ProducerRecord<>(topicItem, itemDTO.getTransaccion().concat("items"), itemDTO);
                producerItems.send(messageItems);

            }


            /*ProducerRecord<String,TransaccionDTO> messageTransaccion=new ProducerRecord<>(topicTransaccion,transaccionDTO.getIdentificadorTransaccion().concat("-123"),transaccionDTO);
            ProducerRecord<String,TransaccionVentaDTO> messageTransaccionVenta=new ProducerRecord<>(topicTransaccionVenta,transaccionVentaDTO.getTransaccion().concat("-123"),transaccionVentaDTO);

            producerTransaccion.send(messageTransaccion);
            producerTransaccionVenta.send(messageTransaccionVenta);*/

        }
       //producerPagos.close();
        producerItems.close();
      /*  producerTransaccion.close();
        producerTransaccionVenta.close();*/


    }


    private static TransaccionDTO createTransaccion(String transaccion) {
        TransaccionDTO transaccionDTO = new TransaccionDTO(
                transaccion,
                "28",
                "1639730160000",
                "640615",
                "6",
                "false",
                "false",
                "false",
                "SALE",
                "20211217",
                "18978",
                "18",
                "11",
                "1639730252000",
                "147427",
                "00001152702199",
                "152702199",
                "1",
                "83",
                "0"
        );
        return transaccionDTO;
    }

    private static TransaccionVentaDTO createTransaccionVenta(String transaccion) {
        TransaccionVentaDTO transaccionVentaDTO = new TransaccionVentaDTO(
                transaccion,
                "43722029",
                "20211217",
                "2",
                "0028-  40368140",
                "1",
                "0",
                "1000",
                "0",
                "0",
                "0"
        );
        return transaccionVentaDTO;
    }

    private static PagoDTO createPago(String transaccion, String medioPago) {
        PagoDTO pagoDTO = new PagoDTO(
                transaccion,
                medioPago,
                "20211217",
                "170447",
                String.valueOf((Math.random()*10000)+1),
                "false"
        );
        return pagoDTO;
    }

    private static ItemDTO createItem(String transaccion, String plu, String ean) {
        ItemDTO itemDTO = new ItemDTO(
                ean,
                plu,
                "10",
                "1",
                "100",
                "19",
                "0",
                "20211217",
                transaccion,
                "19",
                "0",
                "false",
                "1");
        return itemDTO;
    }

    private static MedioPagoDTO createMedioPago(String id, String nombre){
        MedioPagoDTO medioPagoDTO=new MedioPagoDTO(
                id,
                nombre,
                "11",
                "11",
                "0"
        );
        return medioPagoDTO;
    }


    public static class Nifi {
    }
}
