import dto.Item;
import dto.NovedadesVentas;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import serializer.GsonSerializer;

import java.util.Properties;

public class Main {
    public static void main(String[] args) {
        Properties properties = new Properties();
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        properties.put(ProducerConfig.CLIENT_ID_CONFIG,"consumer_test");
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, GsonSerializer.class.getName());

        String topic="streaming.publish.ventascarulla";
        int messagesNumber=100;

        Producer<String, NovedadesVentas> producer = new KafkaProducer<>(properties);
        for(int i=0;i<messagesNumber;i++) {


            NovedadesVentas novedadesVentas = new NovedadesVentas("99741",
                    "1969696011",
                    "0028",
                    "Exito San Nicolas",
                    "Exito",
                    "2021-04-30T11:11:00-05:00",
                    "6",
                    "11",
                    "Puntos");
            int numItems= ((int) (Math.random()*8))+1;
            for (int j=0;j<=numItems;j++) {
                Item item = new Item("124",
                        String.valueOf(j),
                        "10",
                        "0",
                        "0",
                        "0.0",
                        "0.0");
                novedadesVentas.addItem(item);

            }

            ProducerRecord<String,NovedadesVentas> message=new ProducerRecord<>(topic,novedadesVentas.getOrder(),novedadesVentas);
            producer.send(message);
            System.out.println(i);

        }
        producer.close();
    }


}
