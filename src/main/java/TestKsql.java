import dto.*;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import serializer.GsonSerializer;

import java.util.Properties;

public class TestKsql {

    public static void main(String[] args) {
        Properties properties = new Properties();
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, GsonSerializer.class.getName());

        String topicTransaccion = "Transaccion";
        String topicTransaccionVenta = "TransaccionVenta";
        String topicPago = "Pagos";
        String topicItem = "Items";
        String topicMedioPago = "mediosPago";
        //String topicDependence = "streaming.maestradependencias-dev";

        int messagesNumber = 1;

        Producer<String, TransaccionKsql> producerTransaccion = new KafkaProducer<>(properties);
        Producer<String, TransaccionVentaKsql> producerTransaccionVenta = new KafkaProducer<>(properties);
        Producer<String, PagosKsql> producerPagos = new KafkaProducer<>(properties);
        Producer<String, ItemKsql> producerItems = new KafkaProducer<>(properties);
        //Producer<String, DependeceDTO> producerMaestroDependencias = new KafkaProducer<>(properties);
        Producer<String, MedioPagoKsql> mediosPago = new KafkaProducer<>(properties);



        //Dependence
       /* DependeceDTO dependeceDTO=new DependeceDTO("0028","Exito San Nicolas","E");
        ProducerRecord<String,DependeceDTO> dependeceDTOProducerRecord=new ProducerRecord<>(topicDependence,dependeceDTO.getCodigoDepend(),dependeceDTO);
        producerMaestroDependencias.send(dependeceDTOProducerRecord);
        producerMaestroDependencias.close();*/


        //Medios de pago
        String[] pagos={"efectivo","Puntos","Tarjeta Exito"};
        int count=1;
        for (String pago:
             pagos) {
            MedioPagoKsql medioPagoKsql=createMedioPago(String.valueOf(count),pago);
            ProducerRecord<String, MedioPagoKsql> messageMedioPago = new ProducerRecord<>(topicMedioPago, String.valueOf(medioPagoKsql.getId()), medioPagoKsql);
            mediosPago.send(messageMedioPago);
            count++;
        }
        mediosPago.close();
        //Transacciones
        for (int i = 0; i < messagesNumber; i++) {
            int clienteId=((int) (Math.random() * 50)) + 1;
            String[] transccion={"SALE","DBX","ABC"};
            int transaccionId=((int) (Math.random() * 2));
            TransaccionKsql transaccionKsql = createTransaccion(String.valueOf(i),String.valueOf(clienteId),transccion[transaccionId]);
            TransaccionVentaKsql transaccionVentaKsql = createTransaccionVenta(String.valueOf(i),String.valueOf(clienteId));

            //Pagos
            int numPagos = ((int) (Math.random() * 3)) + 1;
            for (int j = 1; j <= numPagos; j++) {
                double pagoRandom=(Math.random() * 100000);
                PagosKsql pagosKsql = createPago(String.valueOf(i),numPagos, pagoRandom);
                ProducerRecord<String, PagosKsql> messagePago = new ProducerRecord<>(topicPago, pagosKsql.getTransaccion().concat("-pagos"), pagosKsql);
                producerPagos.send(messagePago);

            }
            //Items
            int numItems = ((int) (Math.random() * 5)) + 1;
            for (int j = 1; j <= numItems; j++) {
                int quantityRandom= (int) ((Math.random() * 20)+1);
                double priceRandom=(Math.random() * 50000);
                ItemKsql itemKsql=createItem(String.valueOf(i),Double.valueOf(quantityRandom),quantityRandom*priceRandom);
                ProducerRecord<String, ItemKsql> messageItems = new ProducerRecord<>(topicItem, itemKsql.getTransaccion().concat("items"), itemKsql);
                producerItems.send(messageItems);

            }


            ProducerRecord<String,TransaccionKsql> messageTransaccion=new ProducerRecord<>(topicTransaccion,transaccionKsql.getIdentificadorTransaccion().concat("-123"),transaccionKsql);
            ProducerRecord<String,TransaccionVentaKsql> messageTransaccionVenta=new ProducerRecord<>(topicTransaccionVenta,transaccionVentaKsql.getTransaccion().concat("-123"),transaccionVentaKsql);

           producerTransaccion.send(messageTransaccion);
            producerTransaccionVenta.send(messageTransaccionVenta);

        }
        producerPagos.close();
        producerItems.close();
        producerTransaccion.close();
        producerTransaccionVenta.close();

    }


    private static TransaccionKsql createTransaccion(String transaccion,String cliente,String tipoTransaccion) {
        TransaccionKsql transaccionKsql = new TransaccionKsql(
                transaccion,
                cliente,
                tipoTransaccion

        );
        return transaccionKsql;
    }

    private static TransaccionVentaKsql createTransaccionVenta(String transaccion, String tarjetaCliente) {
        TransaccionVentaKsql transaccionVentaKsql = new TransaccionVentaKsql(
                transaccion,
                "43722029"

        );
        return transaccionVentaKsql;
    }

    private static PagosKsql createPago(String transaccion, int medioPago,double valor) {
        PagosKsql pagoKsql = new PagosKsql(
                transaccion,
                medioPago,
                valor
        );
        return pagoKsql;
    }

    private static ItemKsql createItem(String transaccion, double quantity, double price) {
        ItemKsql itemKsql = new ItemKsql(
                transaccion,quantity,price);

        return itemKsql;
    }

    private static MedioPagoKsql createMedioPago(String id, String nombre){
        MedioPagoKsql medioPagoKsql=new MedioPagoKsql(
                id,
                nombre
        );
        return medioPagoKsql;
    }


}

