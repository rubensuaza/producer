import dto.DataNifi;
import dto.Item;
import dto.NovedadesVentas;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import serializer.GsonSerializer;

import java.util.Properties;

public class Nifi {

    public static void main(String[] args) {
        Properties properties = new Properties();
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        //properties.put(ProducerConfig.CLIENT_ID_CONFIG, "task_nifi_1");
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, GsonSerializer.class.getName());

        String topic = "test_nifi";
        int messagesNumber = 1;
        String[] tipoDocumento={"cedula","terjeta de identidad"};
        String[] genero={"masculino","femenino"};
        String[] ciudad={"RIONEGRO","MEDELLIN","MARINILLA","LA CEJA","MANIZALES","CALI","BOGOTA"};

        Producer<String, DataNifi> producer = new KafkaProducer<>(properties);
        for (int i = 0; i < messagesNumber; i++) {
            int number1= (int) (Math.random()*2);
            int number2= (int) (Math.random()*2);
            int number3= (int) (Math.random()*7);
            DataNifi dataNifi=new DataNifi("name_"+i,"lastname_"+i,tipoDocumento[number1],"123"+i,genero[number2],ciudad[number3]);

            System.out.println(dataNifi.getNumeroDocumento());


            ProducerRecord<String, DataNifi> message = new ProducerRecord<>(topic, dataNifi.getNumeroDocumento(), dataNifi);
            producer.send(message);


        }
        producer.close();
    }



}
