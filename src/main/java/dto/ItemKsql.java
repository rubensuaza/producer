package dto;

public class ItemKsql {

    private String Transaccion;
    private double quantity;
    private double price;

    public ItemKsql(String transaccion, double quantuty, double price) {
        Transaccion = transaccion;
        this.quantity = quantuty;
        this.price = price;
    }

    public String getTransaccion() {
        return Transaccion;
    }

    public void setTransaccion(String transaccion) {
        Transaccion = transaccion;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
