package dto;

public class PagosKsql {

    private String Transaccion;
    private int medioPago;
    private double valor;

    public PagosKsql(String transaccion, int medioPago, double valor) {
        Transaccion = transaccion;
        this.medioPago = medioPago;
        this.valor = valor;
    }

    public String getTransaccion() {
        return Transaccion;
    }

    public void setTransaccion(String transaccion) {
        Transaccion = transaccion;
    }

    public int getMedioPago() {
        return medioPago;
    }

    public void setMedioPago(int medioPago) {
        this.medioPago = medioPago;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
