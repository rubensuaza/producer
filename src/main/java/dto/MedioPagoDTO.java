package dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MedioPagoDTO {
    @JsonProperty("IdentificadorMedioPago")
    private String IdentificadorMedioPago;
    @JsonProperty("NombreMedioPago")
    private String NombreMedioPago;
    @JsonProperty("IdentificadorPOS")
    private String IdentificadorPOS;
    @JsonProperty("IdentificadorCorporativo")
    private String IdentificadorCorporativo;
    @JsonProperty("Subcategoria")
    private String Subcategoria;

    public MedioPagoDTO(String paymentTypeID, String nombreMedioPago, String identificadorPOS, String identificadorCorporativo, String subCategoria) {
        this.IdentificadorMedioPago = paymentTypeID;
        this.NombreMedioPago = nombreMedioPago;
        this.IdentificadorPOS = identificadorPOS;
        this.IdentificadorCorporativo = identificadorCorporativo;
        this.Subcategoria = subCategoria;
    }

    public String getIdentificadorMedioPago() {
        return IdentificadorMedioPago;
    }

    public void setIdentificadorMedioPago(String identificadorMedioPago) {
        this.IdentificadorMedioPago = identificadorMedioPago;
    }

    public String getNombreMedioPago() {
        return NombreMedioPago;
    }

    public void setNombreMedioPago(String nombreMedioPago) {
        this.NombreMedioPago = nombreMedioPago;
    }

    public String getIdentificadorPOS() {
        return IdentificadorPOS;
    }

    public void setIdentificadorPOS(String identificadorPOS) {
        this.IdentificadorPOS = identificadorPOS;
    }

    public String getIdentificadorCorporativo() {
        return IdentificadorCorporativo;
    }

    public void setIdentificadorCorporativo(String identificadorCorporativo) {
        this.IdentificadorCorporativo = identificadorCorporativo;
    }

    public String getSubcategoria() {
        return Subcategoria;
    }

    public void setSubcategoria(String subcategoria) {
        this.Subcategoria = subcategoria;
    }
}
