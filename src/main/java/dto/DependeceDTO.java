package dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DependeceDTO {

    @JsonProperty("codigoDepend")
    private String codigoDepend;
    @JsonProperty("descripcion")
    private String descripcion;
    @JsonProperty("cadena")
    private String cadena;

    public DependeceDTO(String dependenceID, String nameDependence, String brandIdentifier) {
        this.codigoDepend = dependenceID;
        this.descripcion = nameDependence;
        this.cadena = brandIdentifier;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setCadena(String cadena) {
        this.cadena = cadena;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getCadena() {
        return cadena;
    }

    public String getCodigoDepend() {
        return codigoDepend;
    }

    public void setCodigoDepend(String codigoDepend) {
        this.codigoDepend = codigoDepend;
    }
}
