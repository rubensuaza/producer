package dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonIgnoreProperties(ignoreUnknown = true)
public class TransaccionVentaDTO {

    @JsonProperty("Transaccion")
    private String Transaccion;
    @JsonProperty("TarjetaCliente")
    private String TarjetaCliente;
    @JsonProperty("Edad")
    private String Edad;
    @JsonProperty("TipoDocumento")
    private String TipoDocumento;
    @JsonProperty("ConsecutivoFiscal")
    private String ConsecutivoFiscal;
    @JsonProperty("CodigoEmpacador")
    private String CodigoEmpacador;
    @JsonProperty("TarjetaEmpleado")
    private String TarjetaEmpleado;
    @JsonProperty("ValorDescuentoEmpleado")
    private String ValorDescuentoEmpleado;
    @JsonProperty("NumeroPedido")
    private String NumeroPedido;
    @JsonProperty("TipoNegocio")
    private String TipoNegocio;
    @JsonProperty("IdentificadorPedido")
    private String IdentificadorPedido;

    public TransaccionVentaDTO(String transaccion, String tarjetaCliente, String edad, String tipodocumento, String consecutivoFiscal, String codigoEmpacador, String tarjetaEmpleado, String valorDescuentoEmpleado, String numeroPedido, String tipoNegocio, String identificadorPedido) {
        this.Transaccion = transaccion;
        this.TarjetaCliente = tarjetaCliente;
        this.Edad = edad;
        this.TipoDocumento = tipodocumento;
        this.ConsecutivoFiscal = consecutivoFiscal;
        this.CodigoEmpacador = codigoEmpacador;
        this.TarjetaEmpleado = tarjetaEmpleado;
        this.ValorDescuentoEmpleado = valorDescuentoEmpleado;
        this.NumeroPedido = numeroPedido;
        this.TipoNegocio = tipoNegocio;
        this.IdentificadorPedido = identificadorPedido;
    }

    public String getTransaccion() {
        return Transaccion;
    }

    public void setTransaccion(String transaccion) {
        this.Transaccion = transaccion;
    }

    public String getTarjetaCliente() {
        return TarjetaCliente;
    }

    public void setTarjetaCliente(String tarjetaCliente) {
        this.TarjetaCliente = tarjetaCliente;
    }

    public String getEdad() {
        return Edad;
    }

    public void setEdad(String edad) {
        this.Edad = edad;
    }

    public String getTipoDocumento() {
        return TipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.TipoDocumento = tipoDocumento;
    }

    public String getConsecutivoFiscal() {
        return ConsecutivoFiscal;
    }

    public void setConsecutivoFiscal(String consecutivoFiscal) {
        this.ConsecutivoFiscal = consecutivoFiscal;
    }

    public String getCodigoEmpacador() {
        return CodigoEmpacador;
    }

    public void setCodigoEmpacador(String codigoEmpacador) {
        this.CodigoEmpacador = codigoEmpacador;
    }

    public String getTarjetaEmpleado() {
        return TarjetaEmpleado;
    }

    public void setTarjetaEmpleado(String tarjetaEmpleado) {
        this.TarjetaEmpleado = tarjetaEmpleado;
    }

    public String getValorDescuentoEmpleado() {
        return ValorDescuentoEmpleado;
    }

    public void setValorDescuentoEmpleado(String valorDescuentoEmpleado) {
        this.ValorDescuentoEmpleado = valorDescuentoEmpleado;
    }

    public String getNumeroPedido() {
        return NumeroPedido;
    }

    public void setNumeroPedido(String numeroPedido) {
        this.NumeroPedido = numeroPedido;
    }

    public String getTipoNegocio() {
        return TipoNegocio;
    }

    public void setTipoNegocio(String tipoNegocio) {
        this.TipoNegocio = tipoNegocio;
    }

    public String getIdentificadorPedido() {
        return IdentificadorPedido;
    }

    public void setIdentificadorPedido(String identificadorPedido) {
        this.IdentificadorPedido = identificadorPedido;
    }
}
