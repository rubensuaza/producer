package dto;

import java.util.ArrayList;
import java.util.List;

public class NovedadesVentas {

    private String order;
    private String customer;
    private String locCD;
    private String locationName;
    private String brand;
    private String timestamp;
    private String puntosTransaccion;
    private String paymentTypeID;
    private String paymentTypeDesc;
    private List<Item> items;

    public NovedadesVentas(String order, String customer, String locCD, String locationName, String brand, String timestamp, String puntosTransaccion, String paymentTypeID, String paymentTypeDesc) {
        this.order = order;
        this.customer = customer;
        this.locCD = locCD;
        this.locationName = locationName;
        this.brand = brand;
        this.timestamp = timestamp;
        this.puntosTransaccion = puntosTransaccion;
        this.paymentTypeID = paymentTypeID;
        this.paymentTypeDesc = paymentTypeDesc;
        items=new ArrayList<>();
    }

    public void addItem(Item item){
        items.add(item);
    }

    public String getOrder() {
        return order;
    }
}
