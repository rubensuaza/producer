package dto;

public class TransaccionKsql {

    private String IdentificadorTransaccion;
    private String cliente;
    private String tipoTransaccion;

    public TransaccionKsql(String identificadorTransaccion, String cliente, String tipoTransaccion) {
        IdentificadorTransaccion = identificadorTransaccion;
        this.cliente = cliente;
        this.tipoTransaccion = tipoTransaccion;
    }

    public String getIdentificadorTransaccion() {
        return IdentificadorTransaccion;
    }

    public void setIdentificadorTransaccion(String identificadorTransaccion) {
        IdentificadorTransaccion = identificadorTransaccion;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getTipoTransaccion() {
        return tipoTransaccion;
    }

    public void setTipoTransaccion(String tipoTransaccion) {
        this.tipoTransaccion = tipoTransaccion;
    }
}
