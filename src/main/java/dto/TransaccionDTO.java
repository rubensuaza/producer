package dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TransaccionDTO {

    @JsonProperty("IdentificadorTransaccion")
    private String IdentificadorTransaccion;
    @JsonProperty("Dependencia")
    private String Dependencia;
    @JsonProperty("FechaTransaccion")
    private String FechaTransaccion;
    @JsonProperty("NutPuntos")
    private String NutPuntos;
    @JsonProperty("PuntosAcumulados")
    private String PuntosAcumulados;
    @JsonProperty("EsAnulada")
    private String EsAnulada;
    @JsonProperty("EsSuspendida")
    private String EsSuspendida;
    @JsonProperty("EsRecuperada")
    private String EsRecuperada;
    @JsonProperty("TipoTransaccion")
    private String TipoTransaccion;
    @JsonProperty("Edad")
    private String Edad;
    @JsonProperty("FechaCierre")
    private String FechaCierre;
    @JsonProperty("Terminal")
    private String Terminal;
    @JsonProperty("CodigoTransaccion")
    private String CodigoTransaccion;
    @JsonProperty("FechaInsercion")
    private String FechaInsercion;
    @JsonProperty("Consecutivo")
    private String Consecutivo;
    @JsonProperty("CedulaCajero")
    private String CedulaCajero;
    @JsonProperty("Cajero")
    private String Cajero;
    @JsonProperty("TiempoVenta")
    private String TiempoVenta;
    @JsonProperty("TiempoPago")
    private String TiempoPago;
    @JsonProperty("PuntosAdicionalesMedioPago")
    private String PuntosAdicionalesMedioPago;

    public TransaccionDTO(String transaccion, String dependencia, String fechaTransaccion, String nutPuntos, String puntosAcumulados, String esAnulada, String esSuspendida, String esRecuperada, String tipoTransaccion, String edad, String fechaCierre, String terminal, String codigoTransaccion, String fechaInsercion, String consecutivo, String cedulaCajero, String cajero, String tiempoVenta, String tiempoPago, String puntosAdicionalesMedioPago) {
        this.IdentificadorTransaccion = transaccion;
        this.Dependencia = dependencia;
        this.FechaTransaccion = fechaTransaccion;
        this.NutPuntos = nutPuntos;
        this.PuntosAcumulados = puntosAcumulados;
        this.EsAnulada = esAnulada;
        this.EsSuspendida = esSuspendida;
        this.EsRecuperada = esRecuperada;
        this.TipoTransaccion = tipoTransaccion;
        this.Edad = edad;
        this.FechaCierre = fechaCierre;
        this.Terminal = terminal;
        this.CodigoTransaccion = codigoTransaccion;
        this.FechaInsercion = fechaInsercion;
        this.Consecutivo = consecutivo;
        this.CedulaCajero = cedulaCajero;
        this.Cajero = cajero;
        this.TiempoVenta = tiempoVenta;
        this.TiempoPago = tiempoPago;
        this.PuntosAdicionalesMedioPago = puntosAdicionalesMedioPago;
    }

    public String getIdentificadorTransaccion() {
        return IdentificadorTransaccion;
    }

    public void setIdentificadorTransaccion(String identificadorTransaccion) {
        this.IdentificadorTransaccion = identificadorTransaccion;
    }

    public String getDependencia() {
        return Dependencia;
    }

    public void setDependencia(String dependencia) {
        this.Dependencia = dependencia;
    }

    public String getFechaTransaccion() {
        return FechaTransaccion;
    }

    public void setFechaTransaccion(String fechaTransaccion) {
        this.FechaTransaccion = fechaTransaccion;
    }

    public String getNutPuntos() {
        return NutPuntos;
    }

    public void setNutPuntos(String nutPuntos) {
        this.NutPuntos = nutPuntos;
    }

    public String getPuntosAcumulados() {
        return PuntosAcumulados;
    }

    public void setPuntosAcumulados(String puntosAcumulados) {
        this.PuntosAcumulados = puntosAcumulados;
    }

    public String getEsAnulada() {
        return EsAnulada;
    }

    public void setEsAnulada(String esAnulada) {
        this.EsAnulada = esAnulada;
    }

    public String getEsSuspendida() {
        return EsSuspendida;
    }

    public void setEsSuspendida(String esSuspendida) {
        this.EsSuspendida = esSuspendida;
    }

    public String getEsRecuperada() {
        return EsRecuperada;
    }

    public void setEsRecuperada(String esRecuperada) {
        this.EsRecuperada = esRecuperada;
    }

    public String getTipoTransaccion() {
        return TipoTransaccion;
    }

    public void setTipoTransaccion(String tipoTransaccion) {
        this.TipoTransaccion = tipoTransaccion;
    }

    public String getEdad() {
        return Edad;
    }

    public void setEdad(String edad) {
        this.Edad = edad;
    }

    public String getFechaCierre() {
        return FechaCierre;
    }

    public void setFechaCierre(String fechaCierre) {
        this.FechaCierre = fechaCierre;
    }

    public String getTerminal() {
        return Terminal;
    }

    public void setTerminal(String terminal) {
        this.Terminal = terminal;
    }

    public String getCodigoTransaccion() {
        return CodigoTransaccion;
    }

    public void setCodigoTransaccion(String codigoTransaccion) {
        this.CodigoTransaccion = codigoTransaccion;
    }

    public String getFechaInsercion() {
        return FechaInsercion;
    }

    public void setFechaInsercion(String fechaInsercion) {
        this.FechaInsercion = fechaInsercion;
    }

    public String getConsecutivo() {
        return Consecutivo;
    }

    public void setConsecutivo(String consecutivo) {
        this.Consecutivo = consecutivo;
    }

    public String getCedulaCajero() {
        return CedulaCajero;
    }

    public void setCedulaCajero(String cedulaCajero) {
        this.CedulaCajero = cedulaCajero;
    }

    public String getCajero() {
        return Cajero;
    }

    public void setCajero(String cajero) {
        this.Cajero = cajero;
    }

    public String getTiempoVenta() {
        return TiempoVenta;
    }

    public void setTiempoVenta(String tiempoVenta) {
        this.TiempoVenta = tiempoVenta;
    }

    public String getTiempoPago() {
        return TiempoPago;
    }

    public void setTiempoPago(String tiempoPago) {
        this.TiempoPago = tiempoPago;
    }

    public String getPuntosAdicionalesMedioPago() {
        return PuntosAdicionalesMedioPago;
    }

    public void setPuntosAdicionalesMedioPago(String puntosAdicionalesMedioPago) {
        this.PuntosAdicionalesMedioPago = puntosAdicionalesMedioPago;
    }
}
