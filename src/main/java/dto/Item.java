package dto;

public class Item {

    private String eanPrincipal;
    private String item;
    private String subLineCD;
    private String quantity;
    private String price;
    private String vlrVentasSinImpuestos;
    private String vlrDescuento;

    public Item(String eanPrincipal, String item, String subLineCD, String quantity, String price, String vlrVentasSinImpuestos, String vlrDescuento) {
        this.eanPrincipal = eanPrincipal;
        this.item = item;
        this.subLineCD = subLineCD;
        this.quantity = quantity;
        this.price = price;
        this.vlrVentasSinImpuestos = vlrVentasSinImpuestos;
        this.vlrDescuento = vlrDescuento;
    }


}
