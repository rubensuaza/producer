package dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ItemDTO {
    @JsonProperty("EAN")
    private String EAN;
    @JsonProperty("Plu")
    private String Plu;
    @JsonProperty("Categoria")
    private String Categoria;
    @JsonProperty("Cantidad")
    private String Cantidad;
    @JsonProperty("ValorVenta")
    private String ValorVenta;
    @JsonProperty("ValorIva")
    private String ValorIva;
    @JsonProperty("ValorDescuentos")
    private String ValorDescuentos;
    @JsonProperty("Edad")
    private String Edad;
    @JsonProperty("Transaccion")
    private String Transaccion;
    @JsonProperty("TarifaIva")
    private String TarifaIva;
    @JsonProperty("ImpoConsumo")
    private String ImpoConsumo;
    @JsonProperty("EsConcesion")
    private String EsConcesion;
    @JsonProperty("IdentificadorItem")
    private String IdentificadorItem;

    public ItemDTO(String ean, String plu, String categoria, String cantidad, String valorVenta, String valorIva, String valorDescuentos, String edad, String transaccion, String tarifaIva, String impoConsumo, String esConcesion, String identificadorItem) {
        this.EAN = ean;
        this.Plu = plu;
        this.Categoria = categoria;
        this.Cantidad = cantidad;
        this.ValorVenta = valorVenta;
        this.ValorIva = valorIva;
        this.ValorDescuentos = valorDescuentos;
        this.Edad = edad;
        this.Transaccion = transaccion;
        this.TarifaIva = tarifaIva;
        this.ImpoConsumo = impoConsumo;
        this.EsConcesion = esConcesion;
        this.IdentificadorItem = identificadorItem;
    }

    public String getEAN() {
        return EAN;
    }

    public void setEAN(String EAN) {
        this.EAN = EAN;
    }

    public String getPlu() {
        return Plu;
    }

    public void setPlu(String plu) {
        this.Plu = plu;
    }

    public String getCategoria() {
        return Categoria;
    }

    public void setCategoria(String categoria) {
        this.Categoria = categoria;
    }

    public String getCantidad() {
        return Cantidad;
    }

    public void setCantidad(String cantidad) {
        this.Cantidad = cantidad;
    }

    public String getValorVenta() {
        return ValorVenta;
    }

    public void setValorVenta(String valorVenta) {
        this.ValorVenta = valorVenta;
    }

    public String getValorIva() {
        return ValorIva;
    }

    public void setValorIva(String valorIva) {
        this.ValorIva = valorIva;
    }

    public String getValorDescuentos() {
        return ValorDescuentos;
    }

    public void setValorDescuentos(String valorDescuentos) {
        this.ValorDescuentos = valorDescuentos;
    }

    public String getEdad() {
        return Edad;
    }

    public void setEdad(String edad) {
        this.Edad = edad;
    }

    public String getTransaccion() {
        return Transaccion;
    }

    public void setTransaccion(String transaccion) {
        this.Transaccion = transaccion;
    }

    public String getTarifaIva() {
        return TarifaIva;
    }

    public void setTarifaIva(String tarifaIva) {
        this.TarifaIva = tarifaIva;
    }

    public String getImpoConsumo() {
        return ImpoConsumo;
    }

    public void setImpoConsumo(String impoConsumo) {
        this.ImpoConsumo = impoConsumo;
    }

    public String getEsConcesion() {
        return EsConcesion;
    }

    public void setEsConcesion(String esConcesion) {
        this.EsConcesion = esConcesion;
    }

    public String getIdentificadorItem() {
        return IdentificadorItem;
    }

    public void setIdentificadorItem(String identificadorItem) {
        this.IdentificadorItem = identificadorItem;
    }
}