package dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PagoDTO {
    @JsonProperty("Transaccion")
    private String Transaccion;
    @JsonProperty("MedioPago")
    private String MedioPago;
    @JsonProperty("Edad")
    private String Edad;
    @JsonProperty("IdentificadorPago")
    private String IdentificadorPago;
    @JsonProperty("ValorPago")
    private String ValorPago;
    @JsonProperty("EsDevuelta")
    private String EsDevuelta;

    public PagoDTO(String transaccion, String medioPago, String edad, String identificadorPago, String valorPago, String esDevuelta) {
        this.Transaccion = transaccion;
        this.MedioPago = medioPago;
        this.Edad = edad;
        this.IdentificadorPago = identificadorPago;
        this.ValorPago = valorPago;
        this.EsDevuelta = esDevuelta;
    }

    public String getTransaccion() {
        return Transaccion;
    }

    public void setTransaccion(String transaccion) {
        this.Transaccion = transaccion;
    }

    public String getMedioPago() {
        return MedioPago;
    }

    public void setMedioPago(String medioPago) {
        this.MedioPago = medioPago;
    }

    public String getEdad() {
        return Edad;
    }

    public void setEdad(String edad) {
        this.Edad = edad;
    }

    public String getIdentificadorPago() {
        return IdentificadorPago;
    }

    public void setIdentificadorPago(String identificadorPago) {
        this.IdentificadorPago = identificadorPago;
    }

    public String getValorPago() {
        return ValorPago;
    }

    public void setValorPago(String valorPago) {
        this.ValorPago = valorPago;
    }

    public String getEsDevuelta() {
        return EsDevuelta;
    }

    public void setEsDevuelta(String esDevuelta) {
        this.EsDevuelta = esDevuelta;
    }

    public static void main(String[] args) {

    }
}
