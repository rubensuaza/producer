package dto;

public class TransaccionVentaKsql {

    private String Transaccion;
    private String tarjetaCliente;

    public TransaccionVentaKsql(String transaccion, String tarjetaCliente) {
        Transaccion = transaccion;
        this.tarjetaCliente = tarjetaCliente;
    }

    public String getTransaccion() {
        return Transaccion;
    }

    public void setTransaccion(String transaccion) {
        Transaccion = transaccion;
    }

    public String getTarjetaCliente() {
        return tarjetaCliente;
    }

    public void setTarjetaCliente(String tarjetaCliente) {
        this.tarjetaCliente = tarjetaCliente;
    }
}
